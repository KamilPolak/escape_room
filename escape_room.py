import images
import os


class EscapeRoom():

    def __init__(self):
        #self.description = description
        #elf.instruction = instruction
        pass

    def instruction(self):
        print('''
        Room - to tell you what room you are in
        Stop - end the game
        Help - look at a list of commands

        When asked where you want to go, respond with the
        first letter of right, left, forward or yes/no+
        '''
            )

        print("press enter/return to continue")

        msg1 = input('>').lower()
        if msg1 == "help":
            return self.instruction()
        elif msg1 == "stop":
            return self.exit_command()
        else:
            return self.intro()

    def start(self):
        print('''
        Welcome to Escape Room game

        Type Help for a list of commands

        Press enter or return to start the game...
        ''')
        msg = input('>').lower()
        if msg == "help":
            return self.instruction()
        else:
            return self.intro()

    def intro(self):
        print('''

        You have just woke up in a ltitle room...
        You're locked an a room and your goal is to find a way out.

        ''')

    def exit_command(self):
        print("GAME OVER")
        exit()


game = EscapeRoom()
game.start()


clear_screen = '''
   ''' * 18



dresserOpened = False
hasKey = False
while True:
    print(images.middle)
    direction = input("> ").lower()
    if direction == 'r':
        if dresserOpened:
            print(images.dresserOpened)
            input("> ")
        else:
            while True:
                print(images.dresserClosed)
                print("The dresser has three drawers. The bottom two are empty and the top one has a 4 letter password lock. Enter password:")
                password = input("> ").lower()
                if password == "lost":
                    dresserOpened = True
                    hasKey = True
                    print(images.dresserOpen)
                    print("The drawer is opened! You found a key! Press enter to continue")
                    input("> ")
                    break
                else:
                    print("Incorrect key, do you want to try again? (n/y)")
                    answer = input(">").lower()
                    if answer == "n":
                        break
                    else:
                        print("Enter password: ")

    elif direction == 'l':
        print(images.painting)
        print("Nice painting :) If you want to go back press enter. But before....Do you see any letters?")
        input("> ")
    elif direction == 'f':
        print(images.doorClosed)
        print("Door is locked. However, I think the key from the dresser could unlock the door. Do you want to try the key?")
        input("> ")
        if hasKey:
            print(images.doorOpen)
            input("> ")
            print(images.complete)
            input("> ")
            break

